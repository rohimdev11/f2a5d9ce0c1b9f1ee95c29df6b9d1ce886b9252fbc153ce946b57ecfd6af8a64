package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"technical-test-stti/src/configs"
	"technical-test-stti/src/controllers"
	"technical-test-stti/src/models"
)

var port = 2022

func main() {
	// initialize the database
	configs.StudentDB[1] = models.Student{ID: 1, Name: "Rohim", Age: 22}
	configs.StudentDB[2] = models.Student{ID: 2, Name: "Kenza", Age: 7}

	http.HandleFunc("/student", controllers.GetStudent)
	http.HandleFunc("/student/detail", controllers.GetStudentByID)
	http.HandleFunc("/student/add", controllers.AddStudent)
	http.HandleFunc("/student/delete", controllers.DeleteStudent)

	log.Print(fmt.Sprintf("Server running at port %d", port))
	// / listen port
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	// print any server-based error messages
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

}
