
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/rohimdev11/technical-test-privy.git
```

Go to the project directory

```bash
  cd F2a5d9ce0c1b9f1ee95c29df6b9d1ce886b9252fbc153ce946b57ecfd6af8a64
```

Install dependencies

```bash
  go get .
  go mod tidy
```

Start the server

```bash
  npm install -g nodemon
```

```bash
  make start
```


## Authors

- [@Nur Rohim](https://github.com/nurrohim11)

