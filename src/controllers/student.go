package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"technical-test-stti/src/configs"
	"technical-test-stti/src/models"
	"technical-test-stti/src/utils"
)

func GetStudent(res http.ResponseWriter, req *http.Request) {

	if req.Method != "GET" {

		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "Check your HTTP method: Invalid HTTP method executed",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusMethodNotAllowed, HandlerMessage)
		return
	}

	var students []models.Student

	for _, student := range configs.StudentDB {
		students = append(students, student)
	}

	// parse the student data into json format
	studentJson, err := json.Marshal(&students)
	if err != nil {
		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "Error parsing the student data",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusInternalServerError, HandlerMessage)
		return
	}

	utils.ReturnJsonResponse(res, http.StatusOK, studentJson)
}

func GetStudentByID(res http.ResponseWriter, req *http.Request) {

	if req.Method != "GET" {
		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "Check your HTTP method: Invalid HTTP method executed",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusMethodNotAllowed, HandlerMessage)
		return
	}

	if _, ok := req.URL.Query()["id"]; !ok {
		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "This method requires the student id",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusInternalServerError, HandlerMessage)
		return
	}

	id := req.URL.Query()["id"][0]
	studentId, err := strconv.Atoi(id)

	student, ok := configs.StudentDB[studentId]
	if !ok {
		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "Requested student not found",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusNotFound, HandlerMessage)
		return
	}

	// parse the student data into json format
	studentJson, err := json.Marshal(&student)
	if err != nil {
		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "Error parsing the student data",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusInternalServerError, HandlerMessage)
		return
	}

	utils.ReturnJsonResponse(res, http.StatusOK, studentJson)
}

func AddStudent(res http.ResponseWriter, req *http.Request) {

	if req.Method != "POST" {
		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "Check your HTTP method: Invalid HTTP method executed",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusMethodNotAllowed, HandlerMessage)
		return
	}

	var student models.Student

	payload := req.Body

	defer req.Body.Close()
	// parse the student data into json format
	err := json.NewDecoder(payload).Decode(&student)
	if err != nil {
		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "Error parsing the student data",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusInternalServerError, HandlerMessage)
		return
	}

	configs.StudentDB[student.ID] = student
	// Add the response return message
	HandlerMessage := []byte(`{
	 "success": true,
	 "message": "student was successfully created",
	 }`)

	utils.ReturnJsonResponse(res, http.StatusCreated, HandlerMessage)
}

func DeleteStudent(res http.ResponseWriter, req *http.Request) {

	if req.Method != "DELETE" {
		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "Check your HTTP method: Invalid HTTP method executed",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusMethodNotAllowed, HandlerMessage)
		return
	}

	if _, ok := req.URL.Query()["id"]; !ok {
		// Add the response return message
		HandlerMessage := []byte(`{
		"success": false,
		"message": "This method requires the student id",
	 }`)

		utils.ReturnJsonResponse(res, http.StatusBadRequest, HandlerMessage)
		return
	}

	id := req.URL.Query()["id"][0]
	studentId, _ := strconv.Atoi(id)

	var students []models.Student

	for _, student := range configs.StudentDB {
		students = append(students, student)
	}

	fmt.Println(removeClient(students, studentId))
	// student, ok := configs.StudentDB[studentId]
	// if !ok {
	// 	// Add the response return message
	// 	HandlerMessage := []byte(`{
	// 	"success": false,
	// 	"message": "Requested student not found",
	//  }`)

	// 	utils.ReturnJsonResponse(res, http.StatusNotFound, HandlerMessage)
	// 	return
	// }
	// parse the student data into json format
	// studentJSON, err := json.Marshal(&student)
	// if err != nil {
	// 	// Add the response return message
	// 	HandlerMessage := []byte(`{
	// 	"success": false,
	// 	"message": "Error parsing the student data"
	//  }`)

	// 	utils.ReturnJsonResponse(res, http.StatusBadRequest, HandlerMessage)
	// 	return
	// }

	utils.ReturnJsonResponse(res, http.StatusOK, nil)
}

func removeClient(clients []models.Student, id int) bool {
	for i := len(clients) - 1; i >= 0; i-- {
		if clients[i].ID == id {
			copy(clients[i:], clients[i+1:])
			clients[len(clients)-1] = models.Student{}
			clients = clients[:len(clients)-1]
			return true

			// copy(clients[i:], clients[i+1:])
			// clients[len(clients)-1] = models.Student{} // or the zero value of T
			// clients = clients[:len(clients)-1]
		}
	}
	return false
}
